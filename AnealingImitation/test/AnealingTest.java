/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import anealingimitation.Solution;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author potter
 */
public class AnealingTest {

    private double[][] K;
    private double[][] K1;

    public AnealingTest() {
    }

    @Before
    public void setUp() {
        K = new double[][]{
            {0, 19, 41, 39, 27, 20},
            {19, 0, 24, 31, 35, 13},
            {41, 24, 0, 20, 41, 22},
            {39, 31, 20, 0, 26, 20},
            {27, 35, 41, 26, 0, 23},
            {20, 13, 22, 20, 23, 0}
        };
        K1 = new double[][]{
            {0, 19, 41, 39, 27},
            {19, 0, 24, 31, 35},
            {41, 24, 0, 20, 41},
            {39, 31, 20, 0, 26},
            {27, 35, 41, 26, 0}
        };
    }

    @Test
    public void solutionP() {
        assertEquals(74.08, Solution.P(15, 50), 0.009);
        assertEquals(81.87, Solution.P(5, 25), 0.009);
        assertEquals(67.03, Solution.P(5, 12.5), 0.009);
    }

    @Test
    public void newRandomRouteTest() {
        Solution sol = new Solution(K);
        for (int i = 0; i < 1000; i++) {
            int[] newRandomRoute = sol.newRandomRoute();
            assertTrue("Lower point is not 1", newRandomRoute[0] == 1);
            assertTrue("Upper point is not 1", newRandomRoute[newRandomRoute.length - 1] == 1);
        }
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixLowerTest() {
        Solution sol = new Solution(K);
        sol.mix(1, 2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixUpperTest() {
        Solution sol = new Solution(K);
        sol.mix(2, sol.getRoute().length);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixUpperLowerTest() {
        Solution sol = new Solution(K);
        sol.mix(1, sol.getRoute().length);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixBLowerTest() {
        Solution sol = new Solution(K);
        sol.mixB(1, 2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixBUpperTest() {
        Solution sol = new Solution(K);
        sol.mixB(2, sol.getRoute().length);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixBUpperLowerTest() {
        Solution sol = new Solution(K);
        sol.mixB(1, sol.getRoute().length);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixTest() {
        Solution sol = new Solution(K);
        sol.setRoute(new int[]{1, 2, 3, 4, 5, 6, 1});
        int[] mix = sol.mix(2, sol.getRoute().length - 1);
        assertTrue("mixTest length", sol.getRoute().length == mix.length);
        assertArrayEquals("mixTest1", mix, new int[]{1, 6, 3, 4, 5, 2, 1});
        assertArrayEquals("mixTest2", sol.mix(4, 4), new int[]{1, 2, 3, 4, 5, 6, 1});
        assertArrayEquals("mixTest3", sol.mix(3, 5), new int[]{1, 2, 5, 4, 3, 6, 1});
        assertArrayEquals("mixTest4", sol.mix(5, 3), new int[]{1, 2, 5, 4, 3, 6, 1});
        
        sol = new Solution(K1);
        sol.setRoute(new int[]{1, 2, 3, 4, 5, 1});
        mix = sol.mix(2, sol.getRoute().length - 1);
        assertTrue("mixTest length", sol.getRoute().length == mix.length);
        assertArrayEquals("mixTest5", mix, new int[]{1, 5, 3, 4, 2, 1});
        assertArrayEquals("mixTest6", sol.mix(4, 4), new int[]{1, 2, 3, 4, 5, 1});
        assertArrayEquals("mixTest7", sol.mix(3, 5), new int[]{1, 2, 5, 4, 3, 1});
        assertArrayEquals("mixTest8", sol.mix(5, 3), new int[]{1, 2, 5, 4, 3, 1});    
        assertArrayEquals("mixTest9", sol.mix(2, 4), new int[]{1, 4, 3, 2, 5, 1});
        assertArrayEquals("mixTest10", sol.mix(4, 2), new int[]{1, 4, 3, 2, 5, 1});
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mixBTest() {
        Solution sol = new Solution(K);
        sol.setRoute(new int[]{1, 2, 3, 4, 5, 6, 1});
        int[] mix = sol.mixB(2, sol.getRoute().length - 1);
        assertTrue("mixBTest length", sol.getRoute().length == mix.length);
        assertArrayEquals("mixBTest1", mix, new int[]{1, 6, 5, 4, 3, 2, 1});
        assertArrayEquals("mixBTest2", sol.mix(4, 4), new int[]{1, 2, 3, 4, 5, 6, 1});
        assertArrayEquals("mixBTest3", sol.mix(3, 5), new int[]{1, 2, 5, 4, 3, 6, 1});
        assertArrayEquals("mixBTest4", sol.mix(5, 3), new int[]{1, 2, 5, 4, 3, 6, 1});
        
        sol = new Solution(K1);
        sol.setRoute(new int[]{1, 2, 3, 4, 5, 1});
        mix = sol.mix(2, sol.getRoute().length - 1);
        assertTrue("mixBTest length", sol.getRoute().length == mix.length);
        assertArrayEquals("mixBTest5", mix, new int[]{1, 5, 4, 3, 2, 1});
        assertArrayEquals("mixBTest6", sol.mix(4, 4), new int[]{1, 2, 3, 4, 5, 1});
        assertArrayEquals("mixBTest7", sol.mix(3, 5), new int[]{1, 2, 5, 4, 3, 1});
        assertArrayEquals("mixBTest8", sol.mix(5, 3), new int[]{1, 2, 5, 4, 3, 1});    
        assertArrayEquals("mixBTest9", sol.mix(2, 4), new int[]{1, 4, 3, 2, 5, 1});
        assertArrayEquals("mixBTest10", sol.mix(4, 2), new int[]{1, 4, 3, 2, 5, 1});        
    }
}

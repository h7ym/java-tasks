/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anealingimitation;

import java.util.Date;
import java.util.Random;

/**
 *
 * @author potter
 */
public class AnealingAlg {
    //исходное значенеи температуры
    private static final double INIT_T = 100.;
    
    //дельта температуры, при достижении которой возможность изменения результата незначительна
    private static final double DELTA_T = 0.0009;
    //коэффициент для получения следующего значения температуры
    //по формуле T_k+1 = T_k * alpha
    private double alpha = 0.5;
    
    private Solution sol = null;
    
    private double T = INIT_T;
    
    private Random r=null;

    public AnealingAlg(Solution initial) {
        sol = initial;
        r = new Random((new Date()).getTime());
    }

    public Solution getSol() {
        return sol;
    }

    /**
     * Задать начальное состояние решения
     * @param sol
     */
    public void setSol(Solution sol) {
        r = new Random((new Date()).getTime());
        this.sol = sol;
        T = INIT_T;
    }

    /**
     * Один раунд поиска решения
     * @return не достигнут ли еще порог темепратуры - DELTA_T
     */
    public boolean round() {
        int i1 = r.nextInt(sol.getRoute().length - 3) + 2;
        int i2;
        do {
            i2 = r.nextInt(sol.getRoute().length - 3) + 2;
        } while (i1 == i2);
        int[] nextSol = sol.mixB(i1, i2);
        
        double dL = sol.L(nextSol)-sol.L();
        
        if (dL > 0) {
            double Pk = Solution.P(dL, T);
            double p = r.nextInt(100);
            if (Pk > p) {
                sol.setRoute(nextSol);
            }
        }
        else sol.setRoute(nextSol);
        
        double dT = T;
        T = alpha * T;
        dT -= T;
        
        return (dT > DELTA_T);
    }

    public double getT() {
        return T;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }
}

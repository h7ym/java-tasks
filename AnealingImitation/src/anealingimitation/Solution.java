/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anealingimitation;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author potter
 */
public class Solution {

    private int[] route = new int[]{1, 4, 3, 5, 6, 2, 1};

    private double[][] K = null;

    public Solution(double[][] K) {
        this.K = new double[K.length][];
        for (int i = 0; i < K.length; i++) {
            this.K[i] = K[i].clone();
        }
        newRandomRoute();
    }

    public int[] newRandomRoute() {
        if (K != null) {
            int[] result = new int[K.length + 1];
            result[0] = 1;
            result[result.length - 1] = 1;
            for (int i = 1; i < result.length - 1; i++) {
                result[i] = i + 1;
            }
            this.route = result;
            Random r = new Random((new Date()).getTime());
            int i1 = r.nextInt(result.length-3) + 2;
            int i2;
            do {
                i2 = r.nextInt(result.length-3) + 2;
            } while (i1 == i2);
            result = this.mix(i1, i2);
            this.setRoute(result);
            return result;
        }
        return null;
    }

    public double L() {
        if (K == null) {
            return 0.0;
        }
        double result = 0;
        for (int i = 1; i < route.length; i++) {
            result += K[route[i] - 1][route[i - 1] - 1];
        }
        return result;
    }

    public double L(int[] newRoute) {
        if (K == null) {
            return 0.0;
        }
        double result = 0;
        for (int i = 1; i < newRoute.length; i++) {
            result += K[newRoute[i] - 1][newRoute[i - 1] - 1];
        }
        return result;
    }

    public int[] mixB(int i1, int i2) {
        if(i1<2 || i2>(route.length-2))
            throw new ArrayIndexOutOfBoundsException("Index out of range 1.."+(route.length-2));        
        int[] result = route.clone();
        int low = i1 < i2 ? i1 : i2;
        int hi = i1 > i2 ? i1 : i2;
        if (low != hi) {
            int[] subroute = new int[hi - low + 1];
            System.arraycopy(result, low - 1, subroute, 0, hi - low + 1);
            for (int i = 0; i < hi - low + 1; i++) {
                result[hi - i - 1] = subroute[i];
            }
        }
        return result;
    }

    public int[] mix(int i1, int i2) {
        if(i1<2 || i2>(route.length-2))
            throw new ArrayIndexOutOfBoundsException("Index out of range 1.."+(route.length-2));
        int[] result = route.clone();
        int low = i1 < i2 ? i1 : i2;
        int hi = i1 > i2 ? i1 : i2;
        if (low != hi) {
            result[low - 1] = route[hi - 1];
            result[hi - 1] = route[low - 1];
        }
        return result;
    }

    public static double P(double dS, double T) {
        return java.lang.Math.exp(-(dS / T)) * 100;
    }

    public int[] getRoute() {
        return route;
    }

    public void setRoute(int[] newRoute) {
        
        if (K == null) {
            throw new RuntimeException("K graph is null");
        }
        if ((newRoute.length - K.length) != 1) {
            throw new RuntimeException("Invalid argumetn length! route.length = " + (K.length + 1) + " expected.");
        }
        this.route = newRoute.clone();
    }

    public double[][] getK() {
        return K;
    }

    public void setK(double[][] K) {
        this.K = new double[K.length][];
        for (int i = 0; i < K.length; i++) {
            this.K[i] = K[i].clone();
        }
    }

    @Override
    public String toString() {
        return "Solution{" + "route=" + Arrays.toString(route) + '}';
    }

}

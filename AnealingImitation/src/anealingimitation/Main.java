/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anealingimitation;

/**
 *
 * @author potter
 */
public class Main {

    /**
     * http://habrahabr.ru/post/209610/
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double[][] k = new double[][]{
            {0, 19, 41, 39, 27, 20},
            {19, 0, 24, 31, 35, 13},
            {41, 24, 0, 20, 41, 22},
            {39, 31, 20, 0, 26, 20},
            {27, 35, 41, 26, 0, 23},
            {20, 13, 22, 20, 23, 0},};
        Solution solution = new Solution(k);
        System.out.println(solution);
//        System.out.println("E> " + solution.getL());
//        solution.setRoute(solution.mix(4, 6));
//        System.out.println(solution);
//        System.out.println("E> " + solution.getL());
//        solution.setRoute(solution.mix(5, 6));
//        System.out.println(solution);
//        System.out.println("E> " + solution.getL());   
        AnealingAlg anealingAlg = new AnealingAlg(solution);
        int i=0;
        do {
            System.out.println("round "+(i++)+", solution = "+anealingAlg.getSol()+", routeL = "+anealingAlg.getSol().L()+", T= "+anealingAlg.getT());
        } while(anealingAlg.round());
    }

}

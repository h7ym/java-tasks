package shidoku;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import shidoku.Solver4x4;

/**
 *
 * @author potter
 */
public class Solver4x4Test {

    private final Integer[][] solvedField = new Integer[][]{
        {2, 1, 4, 3,},
        {4, 3, 2, 1,},
        {3, 2, 1, 4,},
        {1, 4, 3, 2,}
    };

    private final Integer[][] wrongField = new Integer[][]{
        {2, 1, 4, 3,},
        {4, 3, 2, 1,},
        {3, 4, 1, 4,},
        {4, 4, 3, 2,}
    };

    public Solver4x4Test() {
    }

    @Before
    public void setUp() {
    }

    @Test
    public void checkSetUp() {
        assertEquals(solvedField[0][0], new Integer(2));
        assertEquals(solvedField[1][1], new Integer(3));
        assertEquals(solvedField[3][3], new Integer(2));
    }

    @Test @Ignore
    public void checkHor() {
        Solver4x4 solver4x4 = new Solver4x4();
        solver4x4.setField(solvedField);
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                Integer n = solver4x4.getNum(i, j);
                boolean checkHor = solver4x4.checkHoriz(n, i, j);
                assertTrue(String.format("checkHoriz: field[%d][%d] = %d", j, i, n), checkHor);
            }
        }
        /**/
        solver4x4.setField(wrongField);

        {
            int i;
            int j;
            Integer n;
            boolean checkHor;

            j = 2;
            i = 1;
            n = solver4x4.getNum(i, j);
            checkHor = solver4x4.checkHoriz(n, i, j);
            assertFalse(String.format("checkHoriz: field[%d][%d] = %d", j, i, n), checkHor);

            j = 3;
            i = 0;
            n = solver4x4.getNum(i, j);
            checkHor = solver4x4.checkHoriz(n, i, j);
            assertFalse(String.format("checkHoriz: field[%d][%d] = %d", j, i, n), checkHor);

            j = 3;
            i = 1;
            n = solver4x4.getNum(i, j);
            checkHor = solver4x4.checkHoriz(n, i, j);
            assertFalse(String.format("checkHoriz: field[%d][%d] = %d", j, i, n), checkHor);

            j = 2;
            i = 3;
            n = solver4x4.getNum(i, j);
            checkHor = solver4x4.checkHoriz(n, i, j);
            assertFalse(String.format("checkHoriz: field[%d][%d] = %d", j, i, n), checkHor);
        }

        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                Integer n = solver4x4.getNum(i, j);
                boolean checkHoriz = solver4x4.checkHoriz(n, i, j);
                if ((j == 3 && i == 0)
                        || (j == 2 && i == 1)
                        || (j == 3 && i == 1)
                        || (j == 2 && i == 3)) {
                    assertFalse(String.format("checkHoriz false: field[%d][%d] = %d", j, i, n), checkHoriz);
                    System.out.println(String.format("checkHoriz false: field[%d][%d] = %d - ok", j, i, n));
                } else {
                    assertTrue(String.format("checkHoriz: field[%d][%d] = %d", j, i, n), checkHoriz);
                }
            }
        }
    }

    @Test @Ignore
    public void checkVert() {
        Solver4x4 solver4x4 = new Solver4x4();
        solver4x4.setField(solvedField);
        System.out.println(solver4x4);
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                Integer n = solver4x4.getNum(i, j);
                boolean checkVert = solver4x4.checkVert(n, i, j);
                assertTrue(String.format("checkVert: field[%d][%d] = %d", j, i, n), checkVert);
            }
        }

        solver4x4.setField(wrongField);
        System.out.println(solver4x4);

        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                Integer n = solver4x4.getNum(i, j);
                boolean checkVert = solver4x4.checkVert(n, i, j);
                if ((j == 3 && i == 0)
                        || (j == 1 && i == 0)
                        || (j == 2 && i == 1)
                        || (j == 3 && i == 1)) {
                    assertFalse(String.format("checkVert false: field[%d][%d] = %d", j, i, n), checkVert);
                    System.out.println(String.format("checkVert false: field[%d][%d] = %d - ok", j, i, n));
                } else {
                    assertTrue(String.format("checkVert: field[%d][%d] = %d", j, i, n), checkVert);
                }
            }
        }
    }

    @Test
    public void checkRandomFill() {
        Solver4x4 solver4x4 = new Solver4x4();
//        solver4x4.setField(solvedField);
        System.out.println(
            Arrays.deepToString(solver4x4.randomFill())
        );
    }
    
    @Test
    public void mixTest() {
        Solver4x4 solver4x4 = new Solver4x4();
        Integer[] route= new Integer[]{1,2,3,4};
        Integer[] mix;
        mix = solver4x4.mix(route, 0, 1);
        assertArrayEquals("mix 0 1 ",mix, new Integer[]{2,1,3,4});
        mix = solver4x4.mix(route, 1, 0);
        assertArrayEquals("mix 1 0 ",mix, new Integer[]{2,1,3,4});        
        mix = solver4x4.mix(route, 0, 3);
        assertArrayEquals("mix 0 3 ",mix, new Integer[]{4,2,3,1});
        mix = solver4x4.mix(route, 3, 2);
        assertArrayEquals("mix 3 2 ",mix, new Integer[]{1,2,4,3});
        mix = solver4x4.mix(route, 1, 2);
        assertArrayEquals("mix 1 2 ",mix, new Integer[]{1,3,2,4});
        mix = solver4x4.mix(route, 2, 1);
        assertArrayEquals("mix 2 1 ",mix, new Integer[]{1,3,2,4});        
    }
    
    @Test
    public void mixBTest() {
        Solver4x4 solver4x4 = new Solver4x4();
        Integer[] route= new Integer[]{1,2,3,4};
        Integer[] mix;
        mix = solver4x4.mixB(route, 0, 1);
        assertArrayEquals("mixB 0 1 ",mix, new Integer[]{2,1,3,4});
        mix = solver4x4.mixB(route, 0, 1);
        assertArrayEquals("mixB 0 1 ",mix, new Integer[]{2,1,3,4});        
        mix = solver4x4.mixB(route, 3, 2);
        assertArrayEquals("mixB 3 2 ",mix, new Integer[]{1,2,4,3});
        mix = solver4x4.mixB(route, 1, 2);
        assertArrayEquals("mixB 1 2 ",mix, new Integer[]{1,3,2,4});
        mix = solver4x4.mixB(route, 2, 1);
        assertArrayEquals("mixB 2 1 ",mix, new Integer[]{1,3,2,4});         
        mix = solver4x4.mixB(route, 0, 0);
        assertArrayEquals("mixB 0 0 ",mix, new Integer[]{1,2,3,4});        
        mix = solver4x4.mixB(route, 0, 3);
        assertArrayEquals("mixB 0 3 ",mix, new Integer[]{4,3,2,1});                
        mix = solver4x4.mixB(route, 1, 3);
        assertArrayEquals("mixB 1 3 ",mix, new Integer[]{1, 4,3,2});  
        mix = solver4x4.mixB(route, 0, 2);
        assertArrayEquals("mixB 0 2 ",mix, new Integer[]{3,2,1,4});        
    }
    
    @Test
    public void flattenTest() {
        Solver4x4 solver4x4 = new Solver4x4();
        Integer[] flatten = solver4x4.flatten(new Integer[][]{{1},{2,3},{4,5,6},});
        assertArrayEquals(flatten, new Integer[]{1,2,3,4,5,6});
        flatten = solver4x4.flatten(new Integer[][]{{1},{2,3},{4,5,6,7},});
        assertArrayEquals(flatten, new Integer[]{1,2,3,4,5,6,7});    
        
        flatten = solver4x4.flatten(new Integer[][]{{1},});
        assertArrayEquals(flatten, new Integer[]{1});           
    }
    
    @Test
    public void inflateTest() {
        Solver4x4 solver4x4 = new Solver4x4();
        solver4x4.setCells(new Integer[][]{{1},{2,3},{4,5,6},});
        Integer[][] inflate = solver4x4.inflate(new Integer[]{1,2,3,4,5,6}, solver4x4.getCells());
        assertArrayEquals(inflate, solver4x4.getCells());
        inflate = solver4x4.inflate(new Integer[]{2,1,4,3,5,6}, solver4x4.getCells());
        assertArrayEquals(inflate, new Integer[][]{{2},{1,4},{3,5,6},});
    }
}

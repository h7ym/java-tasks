/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shidoku;

import java.util.Date;

/**
 *
 * @author potter
 */
public class Main {

    private Integer[][] field = new Integer[][]{
        {2, 1, null, null,},
        {null, 3, 2, null,},
        {null, null, null, 4,},
        {1, null, null, null,},};

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Integer[][] field1 = new Integer[][]{
            {4, null, null, null,},
            {null, null, null, 2,},
            {2, null, 1, 4,},
            {null, 4, null, null,},};

        Integer[][] field = new Integer[][]{
            {null, null, 1, 2,},
            {2, null, null, null,},
            {null, null, null, 4,},
            {4, 3, null, null,},};

        Solver4x4 solver = new Solver4x4();
        solver.setField(field);

        long start = (new Date()).getTime();

        int ctr = 0;
        double oldScore = 16;
        Integer[][] randomFill;
        Integer[][] currentSolution;
        randomFill = solver.randomFill();
        currentSolution = solver.getSolution(randomFill);
        double score = solver.score(currentSolution);

        while (score > 0.5) {
            ctr++;

            if (score < oldScore) {
                oldScore = score;
                solver.setCells(randomFill);
                solver.mixCells();
                score = solver.score();
            } else {
                randomFill = solver.randomFill();
                currentSolution = solver.getSolution(randomFill);
                score = solver.score(currentSolution);
            }

            if (score < 7) {
                System.out.println("Rounds = " + ctr + ", score = " + score);
                System.out.println(solver);
            }
        }

        long result = (new Date()).getTime() - start;
        System.out.println("Time " + result + "ms, rounds = " + ctr + ", score = " + score);
        System.out.println(solver);
    }

}

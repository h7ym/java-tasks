package shidoku;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author Nikolay Ill.
 */
public class Solver4x4 {

    private Random r = new Random((new Date()).getTime());
    private Integer[][] field = new Integer[][] //[V][H]
    {
        {2, 1, null, null,},
        {null, 3, 2, null,},
        {null, null, null, 4,},
        {1, null, null, null,},};

    private Integer[][] cells = null;

    public Integer[][] randomFill() {
        Integer[][] newCells = new Integer[field.length][];
        for (int j = 0; j < field.length; j++) {
            Integer[] row = field[j];
            Set<Integer> rowNumSet = new HashSet();
            for (Integer num : row) {
                if (num != null) {
                    rowNumSet.add(num);
                }
            }
            if ((row.length - rowNumSet.size()) > 0) {
                newCells[j] = new Integer[row.length - rowNumSet.size()];

                Set<Integer> allNum = new HashSet();
                for (int t = 1; t < field.length + 1; t++) {
                    allNum.add(t);
                }
                allNum.removeAll(rowNumSet);
                int i = 0;
                for (Integer num : allNum) {
                    newCells[j][i++] = num;
                }
                /*
                 if (cells[j] != null) {
                 if (cells[j].length == 2) {// && r.nextBoolean()) {
                 cells[j] = mix(cells[j], 0, 1);
                 } else {
                 int i1 = r.nextInt(cells[j].length);
                 int i2;
                 do {
                 i2 = r.nextInt(cells[j].length);
                 } while (i1 != i2);
                 cells[j] = mixB(cells[j], i1, i2);
                 }
                 }
                 */
            }
        }
//        newCells = mixCells(newCells);
        return mixCells(newCells);
    }

    public void mixCells() {
        cells = this.mixCells(cells);
    }

    protected Integer[] flatten(Integer[][] cells) {
        int len = 0;
        for (int j = 0; j < cells.length; j++) {
            len += cells[j].length;
        }

        Integer[] result = new Integer[len];
        len = 0;
        for (int j = 0; j < cells.length; j++) {
            System.arraycopy(cells[j], 0, result, len, cells[j].length);
            len += cells[j].length;
        }
        return result;
    }

    protected Integer[][] inflate(Integer[] flatCells, Integer[][] cells) {
        Integer[][] result = new Integer[cells.length][];
        int c = 0;
        for (int j = 0; j < cells.length; j++) {
            result[j] = new Integer[cells[j].length];
            for(int i=0; i < result[j].length; i++, c++) {
                result[j][i] = flatCells[c];
            }
        }
        return result;
    }

    private Integer[][] mixCells(Integer[][] cells) {
        Integer[][] result = new Integer[cells.length][];
        /*
         for (int j = 0; j < cells.length; j++) {
         int cl = cells[j].length;

         if(cl == 1) result[j]=cells[j];
         else
         if (cells[j] != null) {
         if (cl == 2 && r.nextBoolean()) {
         result[j] = mix(cells[j], 0, 1);
         } else {
         int i1 = r.nextInt(cl);
         int i2;
         do {
         i2 = r.nextInt(cl);
         } while (i1 == i2);
         result[j] = mixB(cells[j], i1, i2);
         }
         }
         }
         */
        int len = 0;
        for (int j = 0; j < cells.length; j++) {
            result[j] = cells[j].clone();
            len += result[j].length;
        }

        Integer[] shuffle = this.flatten(result);

        int j1;
        j1 = r.nextInt(cells.length);

        int j2;
        do {
            j2 = r.nextInt(cells.length);
        } while (j1 == j2);

        mixB(shuffle, j1, j2);

        return inflate(shuffle, cells);
    }

    public Integer[][] getSolution() {
        return getSolution(this.cells);
    }

    public Integer[][] getSolution(Integer[][] aCells) {
//        System.out.println("cells = "+Arrays.deepToString(cells));
        Integer[][] result = new Integer[field.length][];
        for (int j = 0; j < field.length; j++) {
//            System.out.println("> fj = "+Arrays.toString(field[j]));
            result[j] = field[j].clone();
//            System.out.println("> rj = "+Arrays.toString(field[j]));
            int c = 0;
            Integer[] cellsRow = aCells[j];
            if (cellsRow == null) {
                throw new RuntimeException("cells array row " + j + " is null!");
            }
            for (int i = 0; i < result[j].length; i++) {
                if (result[j][i] == null) {
                    result[j][i] = aCells[j][c++];
                }
            }
//            System.out.println("result j="+j+"; "+Arrays.deepToString(result));
        }
        return result;
    }

    public Integer[][] getSolutionA() {
//        System.out.println("cells = "+Arrays.deepToString(cells));
        Integer[][] result = new Integer[field.length][];
        for (int j = 0; j < field.length; j++) {
//            System.out.println("> fj = "+Arrays.toString(field[j]));
            result[j] = field[j].clone();
//            System.out.println("> rj = "+Arrays.toString(field[j]));
            int c = 0;
            Integer[] cellsRow = cells[j];
            if (cellsRow == null) {
                throw new RuntimeException("cells array row " + j + " is null!");
            }
            for (int i = 0; i < result[j].length; i++) {
                if (result[j][i] == null) {
                    result[j][i] = cells[j][c++];
                }
            }
//            System.out.println("result j="+j+"; "+Arrays.deepToString(result));
        }
        return result;
    }

    public boolean checkHoriz(Integer num, int numX, int y) {
        Integer[] row = field[y];
        for (int i = 0; i < row.length; i++) {
            if (num.equals(row[i]) && i != numX) {
                return false;
            }
        }
        return true;
    }

    public boolean checkVert(Integer num, int x, int numY) {
        for (int j = 0; j < field.length; j++) {
            if (j == numY) {
                continue;
            }
            if (num.equals(field[j][x])) {
                return false;
            }
        }
        return true;
    }

    public double score(Solver4x4 sol) {
        double result = 0;
        Integer[][] solution = sol.getSolution();
        Set<Integer> numSet;

        for (int j = 0; j < solution.length; j++) {
            Integer[] row = solution[j];
            numSet = new HashSet<>();
            for (Integer i : row) {
                if (numSet.contains(i)) {
                    result += 1;
                } else {
                    numSet.add(i);
                }
            }
        }

        for (int i = 0; i < solution.length; i++) {
            numSet = new HashSet<>();
            for (int j = 0; j < solution.length; j++) {
                Integer n = solution[j][i];
                if (numSet.contains(n)) {
                    result += 1;
                } else {
                    numSet.add(n);
                }
            }
        }
        return result;
    }

    public double score(Integer[][] solution) {
        double result = 0;
        Set<Integer> numSet;

        for (int j = 0; j < solution.length; j++) {
            Integer[] row = solution[j];
            numSet = new HashSet<>();
            for (Integer i : row) {
                if (numSet.contains(i)) {
                    result += 1;
                } else {
                    numSet.add(i);
                }
            }
        }

        for (int i = 0; i < solution.length; i++) {
            numSet = new HashSet<>();
            for (int j = 0; j < solution.length; j++) {
                Integer n = solution[j][i];
                if (numSet.contains(n)) {
                    result += 1;
                } else {
                    numSet.add(n);
                }
            }
        }
        return result;
    }

    public double scoreA(Solver4x4 sol) {
        double result = 0;
        Integer[][] solution = sol.getSolution();
        for (int j = 0; j < solution.length; j++) {
            Integer[] row = solution[j];
            for (int i = 0; i < row.length; i++) {
//                System.out.println(String.format("score: field[%d][%d] = %d", j, i, row[i]));
                if (!sol.checkHoriz(row[i], i, j)) {
                    result += 1;
                }
                if (!sol.checkVert(row[i], i, j)) {
                    result += 1;
                }
            }
        }
        return result;
    }

    public double score() {
        return score(this);
    }

    public Integer[][] getField() {
        return field;
    }

    public void setField(Integer[][] field) {
        this.field = field;
    }

    public Integer getNum(int x, int y) {
        return field[y][x];
    }

    public Integer[] mixB(Integer[] route, int i1, int i2) {
        int low = i1 < i2 ? i1 : i2;
        int hi = i1 > i2 ? i1 : i2;
        if (low < 0 || hi >= route.length) {
            throw new ArrayIndexOutOfBoundsException("Index out of range 0.." + route.length);
        }
        Integer[] result = route.clone();
        if (low != hi) {
            Integer[] subroute = new Integer[hi - low + 1];
            System.arraycopy(result, low, subroute, 0, subroute.length);
            for (int i = 0; i < subroute.length; i++) {
                result[hi - i] = subroute[i];
            }
        }
        return result;
    }

    public Integer[] mix(Integer[] route, int i1, int i2) {
        int low = i1 < i2 ? i1 : i2;
        int hi = i1 > i2 ? i1 : i2;
        if (low < 0 || hi >= route.length) {
            throw new ArrayIndexOutOfBoundsException("Index out of range 0.." + route.length);
        }
        Integer[] result = route.clone();
        if (low != hi) {
            result[low] = route[hi];
            result[hi] = route[low];
        }
        return result;
    }

    @Override
    public String toString() {
        Integer[][] solution = getSolution();
        StringBuilder fieldStr = new StringBuilder();
        for (int j = 0; j < solution.length; j++) {
            for (int i = 0; i < solution[0].length; i++) {
                fieldStr.append(solution[j][i]).append(", ");
            }
            fieldStr.append("\n");
        }
        return "Solver4x4{" + "field=\n" + fieldStr.toString() + "}";
    }

    public Integer[][] getCells() {
        return cells;
    }

    public void setCells(Integer[][] cells) {
        this.cells = cells;
    }

}

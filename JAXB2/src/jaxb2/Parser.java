/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxb2;

import java.io.File;
import javax.xml.bind.JAXBException;

/**
 *
 * @author potter
 */
public interface Parser<T> {
    T getObject(File file, Class<T> c) throws JAXBException;
    void saveObject(File file, T o) throws JAXBException;
}

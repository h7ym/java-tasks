/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "person")
@XmlType(propOrder = {"name","age","friends"})
public class Person {
    private String name;
    private int age;
    private List<Person> friends;
 
    public String getName() {
        return name;
    }
 
    @XmlElement
    public void setName(String name) {
        this.name = name;
    }
 
    public int getAge() {
        return age;
    }
 
    @XmlElement
    public void setAge(int age) {
        this.age = age;
    }
 
    public List<Person> getFriends() {
        return friends;
    }
 
    @XmlElement(name = "friend")
    @XmlElementWrapper
    public void setFriends(List<Person> friends) {
        this.friends = friends;
    }
 
    @Override
    public String toString() {
        String res = "Person{" +
                "name='" + name + '\'' +
                ", age=" + age + ", friends{";
        if(friends != null){
            for(Person p : friends){
                res += p.toString();
            }
        }
        res += "}}";
        return res;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.name);
        hash = 11 * hash + this.age;
        hash = 11 * hash + Objects.hashCode(this.friends);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.friends, other.friends)) {
            return false;
        }
        return true;
    }


    
    
}

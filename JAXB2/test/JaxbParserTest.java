/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import entity.Person;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import jaxb2.JAXBParser;
import jaxb2.Parser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author potter
 */
public class JaxbParserTest {

    private Parser<Person> parser;
    private File file;

    @Before
    public void setUp() {
        parser = new JAXBParser<>();
        file = new File("person.xml");
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testGetObject() throws Exception {
        Person person = parser.getObject(file, Person.class);
        System.out.println(person);
    }

    @Test
    public void testSaveObject() throws Exception {
        Person person = new Person();
        person.setName("Oleg");
        person.setAge(19);

        Person friend1 = new Person();
        Person friend2 = new Person();

        friend1.setName("Viktor");
        friend1.setAge(20);

        friend2.setName("Stepan");
        friend2.setAge(15);

        List<Person> friends = new ArrayList<>();
        friends.add(friend1);
        friends.add(friend2);

        person.setFriends(friends);

        parser.saveObject(file, person);
    }
}

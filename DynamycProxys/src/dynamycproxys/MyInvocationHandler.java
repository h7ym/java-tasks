/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamycproxys;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 *
 * @author potter
 */
class MyInvocationHandler implements InvocationHandler {

    private MyInterface obj = null;

    public MyInvocationHandler(MyInterface target) {
        obj = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        System.out.println("P> "+proxy.toString());
        Object result = null;
        String mname = method.getName();
        System.out.println("M> " + mname);
        if (args != null && args.length > 0) {
            System.out.println("..." + args.length);
            for (Object o : args) {
                System.out.println("O> " + o);
            }
        }
        System.out.println("...");
        System.out.println(String.format("invoke %s.%s", obj.getClass().getSimpleName(), mname));
        result = method.invoke(obj, args);
        System.out.println("finished!");
        return result;
    }

}

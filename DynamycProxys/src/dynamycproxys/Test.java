/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dynamycproxys;

/**
 *
 * @author potter
 */
public class Test implements MyInterface {
    private String str="Test";

    @Override
    public void test1() {
        System.out.println("test1");
    }

    @Override
    public String getStr() {
        return str;
    }

    @Override
    public void setStr(String str) {
        System.out.println("Test.setStr: "+str);
        this.str = str;
    }
    
}

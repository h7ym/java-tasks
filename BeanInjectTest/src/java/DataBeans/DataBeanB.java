/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBeans;

/**
 *
 * @author my7h
 */
public class DataBeanB implements IDataBeanB {
    private String model=null;
    private int size=0;

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        System.out.println("setter fired "+model);
        this.model = model;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        System.out.println("setter fired "+size);        
        this.size = size;
    }
    
    
}

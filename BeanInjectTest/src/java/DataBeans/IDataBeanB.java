/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBeans;

/**
 *
 * @author my7h
 */
public interface IDataBeanB extends IDataBean {

    String getModel();

    int getSize();

    void setModel(String model);

    void setSize(int size);
    
}

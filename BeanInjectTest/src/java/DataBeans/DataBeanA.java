/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBeans;

/**
 *
 * @author my7h
 */
public class DataBeanA implements IDataBean, IDataBeanA {
    private String name=null;
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        System.out.println("setter fired "+name);
        this.name = name;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 *
 * @author my7h
 */
public enum DataBeanType {
    DBA("DataBeanA"),DBB("DataBeanB");
    
    private final String value;
    DataBeanType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DataBeans.DataBeanA;
import DataBeans.DataBeanB;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import DataBeans.IDataBean;
import javax.enterprise.context.RequestScoped;
/**
 *
 * @author my7h
 */
@Named
@SessionScoped
public class DataBeansProducer implements Serializable {
    private IDataBean db = new DataBeanA();
        
    @Produces
    @Named(value = "dataBean")
//    @RequestScoped
    public IDataBean getDataBean()
    {
        System.out.println("get data bean "+db.getClass().getSimpleName());
        return db;
    }
    
    public String selectDBA() {System.out.println("dba created");db=new DataBeanA(); return "/faces/dba.xhtml";}
    public String selectDBB() {System.out.println("dbb created");db=new DataBeanB(); return "/faces/dbb.xhtml";}
       
}

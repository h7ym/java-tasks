/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxb1;

import com.ibm.dw.Guitars;

/**
 *
 * @author potter
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        RoundTripper roundTripper = new RoundTripper(args[0], args[1]);
        Guitars guitars = roundTripper.unmarshal();
        roundTripper.marshal(guitars);
    }
    
}

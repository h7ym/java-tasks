/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxb1;

import com.ibm.dw.Guitars;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author potter
 */
public class RoundTripper {

    private String inputFilename;
    private String outputFilename;
    private JAXBContext jc;

    private final String PACKAGE_NAME = "com.ibm.dw";

    public RoundTripper(String inputFilename, String outputFilename) throws Exception {
        this.inputFilename = inputFilename;
        this.outputFilename = outputFilename;
        jc = JAXBContext.newInstance(PACKAGE_NAME);
    }

    public void marshal(Guitars guitars) throws Exception {
        Marshaller m = jc.createMarshaller();
        m.marshal(guitars, new FileOutputStream(outputFilename));
    }

    public Guitars unmarshal() throws Exception {
        Unmarshaller u = jc.createUnmarshaller();
        return (Guitars) u.unmarshal(new FileInputStream(inputFilename));
    }

//    public static void main(String[] args) {
//        if (args.length < 2) {
//            System.err.println("Incorrect usage: java RoundTripper"
//                    + "[input XML filename] [output XML filename]");
//            return;
//        }
//
//        try {
//            RoundTripper rt = new RoundTripper(args[0], args[1]);
//            Guitars guitars = rt.unmarshal();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return;
//        }
//    }
}

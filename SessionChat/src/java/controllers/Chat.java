/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author my7h
 */
@Named
@SessionScoped
public class Chat implements Serializable {

    private static final List<String> messages = Collections.synchronizedList(new LinkedList());
    private int lsize = -1;

    private String uname = "anon";

    {
        Date date = new Date();
        uname = String.format("%s%X", uname, date.getTime());
    }

    public boolean isEmpty() {
        return messages.isEmpty();
    }

    public boolean add(String e) {
        if (messages.add(e)) {            
            lsize = messages.size();
            return true;
        }
        return false;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }
    
    public String getMessages() {
        StringBuilder sb = new StringBuilder("");
        for(String s: messages) {
            sb.append(s).append('\n');
        }
        return sb.toString();
    }
    
    public void updateChat() {
        System.out.println("> "+this.getMessages());
    }
    
    public void setMessage(String msg) {
        System.out.println("+ "+msg);
        this.add(uname+": "+msg);
    }
    
    public String getMessage() {return "";}
}

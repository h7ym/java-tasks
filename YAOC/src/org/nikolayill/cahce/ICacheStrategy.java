/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

/**
 *
 * @author my7h
 */
interface ICacheStrategy <K> {
    K getNextToEvict();
    K evictNext();
    boolean putEntry(K key);
    void hitEntry(K key); 
    void removeEntry(K key);
    void clearQueue();
    void setSize(int size);
    int getSize();
}

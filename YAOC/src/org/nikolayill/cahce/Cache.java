/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.io.Serializable;

/**
 *
 * @author my7h
 * @param <K>
 * @param <V>
 */
public class Cache<K, V extends Serializable> implements ICache<K, V> {

    private ICacheStrategy<K> cacheStrategy = null;
    private ICacheStorage<K, V> cacheStorage = null;
    private ICache<K,V> subCache = null;

    public Cache(ICacheStrategy<K> cacheStrategy, ICacheStorage<K, V> cacheStorage) {
        setStrategy(cacheStrategy);
        setStorage(cacheStorage);
    }
    
    @Override
    public void evict(K key) {
        if(subCache!=null) {
            subCache.evict(key);
//            V object = get(key);
//            if(object!=null) subCache.put(key, object);            
        }
        cacheStrategy.removeEntry(key);
        cacheStorage.remove(key);
    }

    @Override
    public void evictAll() {
        if(subCache!=null) subCache.evictAll();
        cacheStrategy.clearQueue();
        cacheStorage.clear();
    }

    @Override
    public V get(K key) {
        V val = cacheStorage.get(key);
        if(val!=null) cacheStrategy.hitEntry(key);
        else if (subCache!=null) {
            System.out.println("no value for key "+key+" in cache!");
            val = subCache.get(key);
            if(val!=null) {
                System.out.println("value from sub cache "+val);
                subCache.evict(key);
                put(key, val);
            }
        }
        System.out.println("cache get "+val+" for key "+key);
        return val;
    }

    @Override
    public void put(K key, V value) {
        while(!cacheStrategy.putEntry(key)) {
            K evictedKey = cacheStrategy.evictNext();
            if (evictedKey != null) {
                V evictedValue = get(evictedKey);
                evict(evictedKey);
                if(subCache!=null) {
                    System.out.println("put value ["+evictedValue+"] in sub cache for key "+evictedKey);
                    subCache.put(evictedKey, evictedValue);
                }
            }
        }
        cacheStorage.put(key, value);
    }

    void setStrategy(ICacheStrategy<K> cacheStrategy) {
        this.cacheStrategy = cacheStrategy;
    }

    void setStorage(ICacheStorage<K, V> cacheStorage) {
        this.cacheStorage = cacheStorage;
    }

    @Override
    public void setSubCache(ICache<K, V> subCache) {
        this.subCache = subCache;
    }        
}

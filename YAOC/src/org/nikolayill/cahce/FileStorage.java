/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author potter
 * @param <K>
 * @param <V>
 */
public class FileStorage<K, V extends Serializable> implements ICacheStorage<K,V> {
    private String cachePath = "";
    private final Map<K, String> cacheNames = new HashMap<>();

    FileStorage() {
        this("");
    }

    private String getUniqueName() {
        return UUID.randomUUID().toString();
    }
    
    @SuppressWarnings("unchecked")
    private V readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        return (V)ois.readObject();
    }
    
    @Override    
    public V get(K key) {
        if (!cacheNames.containsKey(key)) return null;
        String uname = cacheNames.get(key);
        File cacheFile = new File(cachePath, uname);
        V result = null;
        if (cacheFile.exists()) {
            System.out.println("file exists");
            ObjectInputStream ois = null;
            FileInputStream is = null;
            try {
                is = new FileInputStream(cacheFile);
                ois = new ObjectInputStream(is);                
                result = (V)ois.readObject();
                System.out.println("read result "+result);
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(FileStorage.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if(ois!=null) ois.close();
                    else if(is!=null) is.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileStorage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            cacheNames.remove(key);
            Logger.getLogger(FileStorage.class.getName()).log(Level.SEVERE, null, "File " + uname + " not found, name entry removed!");
        }
        return result;
    }

    @Override
    public void put(K key, V value) {
        String uname = null;
        if (cacheNames.containsKey(key)) {
            uname = cacheNames.get(key);
        }
        else {
            uname = this.getUniqueName();
            cacheNames.put(key, uname);
        }
        File cacheFile = new File(cachePath, uname);
        ObjectOutputStream oos=null;
        FileOutputStream fos = null;            
        try {            
            fos = new FileOutputStream(cacheFile, false);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(value);
        } catch (IOException ex) {
            Logger.getLogger(FileStorage.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(oos!=null) oos.close();
                else if(fos!=null) fos.close();
            } catch (IOException ex) {
                Logger.getLogger(FileStorage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public boolean contains(K key) {
        boolean result = false;
        if(this.cacheNames.containsKey(key)) {
            String uname = cacheNames.get(key);
            File cacheFile = new File(cachePath, uname);
            result = cacheFile.exists();
        }
        System.out.println("contains "+key+" "+result);
        return result;
    }

    @Override
    public void remove(K key) {
        String uname = null;
        if (cacheNames.containsKey(key)) {
            uname = cacheNames.get(key);
            cacheNames.remove(key);
            File cacheFile = new File(cachePath, uname);
            if(cacheFile.exists()) {
                if(!cacheFile.delete()) System.err.println("R>Can't delete file "+ uname);
            }
            else System.err.println("R>File not exists "+ uname);
        }
    }

    @Override
    public void clear() {
        for(String uname: cacheNames.values()) {
            File cacheFile = new File(cachePath, uname);
            if(cacheFile.exists()) {
                if(!cacheFile.delete()) System.err.println("C>Can't delete file "+ uname);
            }
            else System.err.println("C>File not exists "+ uname);
        }
    }

    @Override
    public int size() {
        return cacheNames.size();
    }

    public String getCachePath() {
        return cachePath;
    }

    private void setCachePath(String cachePath) {
        this.cachePath = cachePath;
    }    

    public FileStorage(String cachePath) {
        setCachePath(cachePath);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

/**
 *
 * @author potter
 */
public enum CacheStorage {
    INMEM_CACHE(HashMapStorage.class), FS_CACHE(FileStorage.class);
    
    private final Class<? extends ICacheStorage> storage;
    private String path="";
    
    CacheStorage(Class<? extends ICacheStorage> storage) {
        this.storage=storage;
    }

    public Class<? extends ICacheStorage> getStorage() {
        return storage;
    }        
    
    public CacheStorage path(String path) {
        this.path=path;
        return this;
    }
}

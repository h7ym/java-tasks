/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.util.LinkedList;


/**
 *
 * @author potter
 * @param <K>
 */
public class CacheFIFOStrategy<K> implements ICacheStrategy<K> {
    private LinkedList<K> queue = new LinkedList<>();
    private int size = 1000;
    
    @Override
    public K getNextToEvict() {
        if(queue.size()>=size) return queue.getFirst();
        else return null;
    }
    
    @Override
    public K evictNext() {
        K result = getNextToEvict();
        if(result!=null) queue.removeFirst();
        return result;
    }
    
    @Override
    public void hitEntry(K key) {
        if(queue.contains(key)) {
            queue.remove(key);
            queue.addLast(key);
        }
    }       

    @Override
    public void removeEntry(K key) {
        queue.remove(key);
    }

    @Override
    public void clearQueue() {
        queue.clear();
    }

    @Override
    public void setSize(int size) {
        this.size=size;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public boolean putEntry(K key) {
        boolean result = queue.size()<size;
        if(result) queue.addLast(key);        
        return result;
    }

    protected LinkedList<K> getQueue() {
        return queue;
    }
}

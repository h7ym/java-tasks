/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.util.LinkedList;

/**
 *
 * @author potter
 * @param <K>
 */
public class CacheLRUStrategy<K> extends CacheFIFOStrategy<K> {

    @Override
    public void hitEntry(K key) {
        if (this.getQueue().contains(key)) {
            LinkedList<K> queue = this.getQueue();
            int keyIndex = queue.indexOf(key);
            if (keyIndex != (queue.size() - 1)) {
                queue.remove(keyIndex);
                queue.add(keyIndex + 1, key);
            }
        }
    }

}

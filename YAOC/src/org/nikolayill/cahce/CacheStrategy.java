/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

/**
 *
 * @author potter
 */
public enum CacheStrategy {    
    FIFO(CacheFIFOStrategy.class), LRU(CacheLRUStrategy.class);
    
    private final Class<? extends ICacheStrategy> strategyClass;
    private CacheStrategy(Class<? extends ICacheStrategy> strategy) {
        this.strategyClass=strategy;
    };

    public Class<? extends ICacheStrategy> getStrategyClass() {
        return strategyClass;
    }    
}

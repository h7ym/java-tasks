/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author my7h
 */
public class CacheFactory {

    public static <K, V extends Serializable> ICache<K, V> getCache(CacheStrategy strategyType, CacheStorage storageType, int size) {
        ICache<K, V> result = null;

        ICacheStorage<K, V> storageInstance = null;
        ICacheStrategy<K> strategyInstance = null;
        Class<? extends ICacheStrategy> strategyClass = strategyType.getStrategyClass();
        Class<? extends ICacheStorage> storageClass = storageType.<ICacheStorage<K,V>>getStorage();

        try {
            storageInstance = storageClass.<K, V>newInstance();
            strategyInstance = strategyClass.<K>newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(CacheFactory.class.getName()).log(Level.SEVERE, null, ex);
        }


//        switch(strategyType) {
//            case FIFO:
//                strategyInstance = new CacheFIFOStrategy<>();
//                break;
//            case LRU:
//                strategyInstance = new CacheLRUStrategy<>();
//                break;
//            default:
//                throw new AssertionError(strategyType.name());
//        }
//        
//        switch(storageType) {
//            case INMEM_CACHE:
//                storageInstance = new HashMapStorage<>();
//                break;
//            case FS_CACHE:
//                storageInstance = new FileStorage<>();
//                break;
//            default:
//                throw new AssertionError(storageType.name());            
//        }
        if (storageInstance != null && strategyInstance != null) {
            strategyInstance.setSize(size);
            result = new Cache<>(strategyInstance, storageInstance);
        }        
        return result;
    }
    
    public static <K, V extends Serializable> ICache<K, V> getCacheStack(ICache<K,V> level1, ICache<K,V> level2) {
        level1.evictAll();
        level2.evictAll();
        
        level1.setSubCache(level2);
        return level1;
    }
}

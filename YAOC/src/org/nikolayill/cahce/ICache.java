/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.io.Serializable;

/**
 *
 * @author my7h
 * @param <K>
 * @param <V>
 */
public interface ICache<K, V extends Serializable> {
    public void evict(K key);
    public void evictAll();
    public V get(K key);   
//    public V getOrElse(K key, IReadValueCommand getCmd);
    public void put(K key, V value);
    public void setSubCache(ICache<K, V> subCache);
//    void setStrategy(ICacheStrategy cs);
}

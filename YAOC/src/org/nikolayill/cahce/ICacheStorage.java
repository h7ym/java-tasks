/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.io.Serializable;

/**
 *
 * @author my7h
 * @param <K>
 * @param <V>
 */
public interface ICacheStorage<K, V extends Serializable>{
    V get(K key);
    void put(K key, V value);
    boolean contains(K key);
    void remove(K key);
    void clear();
    int size();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nikolayill.cahce;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author potter
 * @param <K>
 * @param <V>
 */
public class HashMapStorage<K, V extends Serializable> implements ICacheStorage<K,V> {
    private final Map<K,V> storage = new HashMap<>();
    
    @Override
    public V get(K key) {
        return storage.get(key);
    }

    @Override
    public void put(K key, V value) {
        storage.put(key, value);
    }

    @Override
    public boolean contains(K key) {
        return storage.containsKey(key);
    }

    @Override
    public void remove(K key) {
        storage.remove(key);
    }

    @Override
    public void clear() {
        storage.clear();
    }

    @Override
    public int size() {
        return storage.size();
    }
    
}

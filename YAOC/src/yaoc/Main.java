/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yaoc;

import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.nikolayill.cahce.CacheFactory;
import static org.nikolayill.cahce.CacheStorage.FS_CACHE;
import static org.nikolayill.cahce.CacheStorage.INMEM_CACHE;
import static org.nikolayill.cahce.CacheStrategy.FIFO;
import org.nikolayill.cahce.ICache;

/**
 *
 * @author my7h
 */
public class Main {

    public static String tooLongToGetString(Integer key) {
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return String.format("String for %d key", key);
    }
    
    public static String test(Object str) {
        return (String)str;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        FIFOCache<Integer, String> fifoCache = new FIFOCache<>();
        ICache<Integer, String> cache = CacheFactory.<Integer, String>getCacheStack(
                CacheFactory.<Integer, String>getCache(FIFO, INMEM_CACHE, 5),
                CacheFactory.<Integer, String>getCache(FIFO, INMEM_CACHE, 6));
//        ICache<Integer, String> cache = CacheFactory.<Integer, String>getCache(FIFO, FS_CACHE.path("E:\\chache\\"), 10);
//        ICache<Integer, String>
        Integer[] keysSet1 = 
            {0, 8, 1, 10, 10, 0, 2, 8, 3, 4, 5, 6, 2, 2, 5, 9, 4, 10, 6, 6, 3, 
                2, 8, 7, 4, 2, 0, 6, 10, 4, 10, 10, 0, 7, 1, 9, 5, 7, 1, 7, 6, 
                1, 5, 1, 3, 0, 3, 2, 1, 7, 3, 1, 2, 5, 9, 1, 9, 4, 5, 9, 9, 1, 
                1, 5, 10, 3, 9, 2, 3, 8, 9, 3, 7, 3, 8, 3, 9, 0, 4, 7, 1, 6, 8, 
                0, 0, 1, 2, 5, 1, 1, 9, 8, 8, 8, 7, 5, 8, 2, 1, 10};
        Integer[] keysSet2 = {10, 5, 6, 4, 6, 5, 3, 1, 2, 3, 7, 2, 1, 7, 1, 8, 
            4, 2, 1, 6, 10, 9, 3, 8, 1, 6, 9, 10, 4, 4, 2, 7, 10, 5, 4, 8, 6, 
            8, 6, 9, 4, 3, 0, 9, 8, 1, 7, 7, 6, 0, 3, 8, 10, 1, 8, 3, 3, 7, 4, 
            0, 5, 7, 4, 9, 0, 0, 2, 9, 5, 7, 10, 1, 5, 5, 2, 8, 2, 3, 10, 10, 
            10, 5, 8, 6, 5, 7, 9, 2, 0, 9, 1, 8, 6, 2, 6, 3, 2, 5, 3, 1};
        Integer[] keysSet3 = {6,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,6,7,8,9,10,11};
        Integer[] keys = keysSet2;
        String value = null;
        Integer key = 0;
        Integer factor = 11;
        Random random = new Random();
//        System.out.print("{");
//        for(int k=0; k<100; k++) {System.out.print(", "+random.nextInt(11));}
//        System.out.println("}");
        long mllisTotal = (new Date()).getTime();
        int ctr = 0;
        for(Integer kv: keys){
//        for(int k=0; k<100; k++) {
//            key = random.nextInt(11);
//            key = k%factor;
            key=kv;
            System.out.print("["+(ctr++)+"] ");
            long mllis = (new Date()).getTime();
            if((value=cache.get(key))!=null) {
                System.out.printf("[Value from cache for key %d] %s\n",key, value);
            }
            else {
                value = tooLongToGetString(key);
                cache.put(key, value);
                System.out.printf("[Evolving value for key %d] %s\n",key,value);
            }        
            long delay = (new Date()).getTime() - mllis;
            System.out.println("[...for "+delay+"ms]");
        }
        long delayTotal = (new Date()).getTime() - mllisTotal;
        System.out.println("[Total time "+delayTotal+"ms]");
        //3236 for LRU
        //3600 for FIFO
        cache.evictAll();
    }
    
}

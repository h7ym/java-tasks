/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication29;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author potter
 */
public class JavaApplication29 {

    public static void eval(int[] data, int p) {
        try {
            for(int i=0; i<data.length; i++) {            
                    data[i]=(int)java.lang.Math.pow(data[i], p);
                    Thread.sleep(1);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(JavaApplication29.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static void main(String[] args) throws InterruptedException {
        final int numcores = 4;        
        ExecutorService pool = Executors.newFixedThreadPool(numcores);
        final int[] data = {1,3,5,7,9,2,4,6,8,10, 1,3,5,7,9,2,4,6,8,10, 1,3,5,7,9,2,4,6,8,10, 1,3,5,7,9,2,4,6,8,10,11};
        final int p = 2;
        
        final int chunk = data.length/numcores;        
        for(int l=0; l<data.length; l+=chunk) {
            final int startPtr = l;
            final int len = (data.length-l)<chunk?(data.length-l):chunk;
            final int[] localData = new int[len];
            
            System.arraycopy(data, l, localData, 0, len);

            Runnable r = new Runnable() {
            @Override
            public void run() {
                    eval(localData, p);
                    System.arraycopy(localData, 0, data, startPtr, len);
            }            
            };
            pool.execute(r);            
        }
        System.out.println("check");
        while(!pool.isTerminated()) Thread.sleep(1);
        for(int a : data) System.out.println(a);        
    }
    
}

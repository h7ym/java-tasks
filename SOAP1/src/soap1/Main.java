/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soap1;

import java.util.List;
import net.yandex.speller.services.spellservice.CheckTextRequest;
import net.yandex.speller.services.spellservice.CheckTextResponse;
import net.yandex.speller.services.spellservice.SpellError;
import net.yandex.speller.services.spellservice.SpellService;
import net.yandex.speller.services.spellservice.SpellServiceSoap;

/**
 *
 * @author potter
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SpellService service = new SpellService();
        SpellServiceSoap port = service.getSpellServiceSoap();
        CheckTextRequest request = new CheckTextRequest();
        request.setLang("ru");
        request.setText("Пама мяла раму");
        CheckTextResponse checkTextResponse = port.checkText(request);
        List<SpellError> errorList = checkTextResponse.getSpellResult().getError();
        System.out.println(errorList.size());
        for (SpellError error : errorList) {
            System.out.println(error.getWord());
        }
    }
    
}

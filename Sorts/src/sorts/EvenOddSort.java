/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorts;

/**
 *
 * @author potter
 * @param <T>
 */
public class EvenOddSort<T extends Comparable<T>> extends SwapSort<T> {

    @Override
    public T[] sort(T[] arr) {
        boolean hasSwaps=true;
        while(hasSwaps) {
            hasSwaps=false;
            for(int i=0; i<arr.length; i+=2) {
                if(arr[i].compareTo(arr[i+1])>0) {
                    swap(arr, i, i+1);
                    hasSwaps=true;
                }
            }
            for(int i=1; i<arr.length; i+=2) {
                if(arr[i].compareTo(arr[i+1])>0) {
                    swap(arr, i, i+1);
                    hasSwaps=true;
                }
            }            
        }
        return arr;
    }
    
}

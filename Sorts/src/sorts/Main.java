/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorts;

import java.util.Arrays;

/**
 *
 * @author potter
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Integer[] arr = new Integer[]{2,1,4,5,0,-5,-7};
        System.out.println(Arrays.toString(arr));
        ISort<Integer> sort = new EvenOddSort<>();
        sort.sort(arr);
        System.out.println(Arrays.toString(arr));
    }
    
}

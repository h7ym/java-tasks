/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorts;

/**
 *
 * @author potter
 * @param <T>
 */
public class ShakeSort<T extends Comparable<T>> extends SwapSort<T> {

    @Override
    public T[] sort(T[] arr) {
        int t=0;
        for(int b=arr.length-1; b>arr.length/2; b--) {
            for(int i=t; i<b; i++) {
                if(arr[i].compareTo(arr[i+1])>0) 
                    swap(arr, i, i+1);
            }
            for(int i=b-2; i>=t; i--) {
                if(arr[i].compareTo(arr[i+1])>0) 
                    swap(arr, i+1, i);
            }          
            t++;
        }
        return arr;
    }
    
}

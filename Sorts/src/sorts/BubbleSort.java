/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorts;

/**
 *
 * @author potter
 * @param <T>
 */
public class BubbleSort<T extends Comparable<T>> extends SwapSort<T> {
    
    @Override
    public T[] sort(T[] arr) {
        for(int b=arr.length-1; b>0; b--) 
            for(int i=0; i<b; i++) {
                if(arr[i].compareTo(arr[i+1])>0) 
                    swap(arr, i, i+1);
            }
        return arr;
    }
    
}

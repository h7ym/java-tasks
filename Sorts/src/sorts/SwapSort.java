/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorts;

/**
 *
 * @author potter
 * @param <T>
 */
public abstract class SwapSort<T extends Comparable<T>> implements ISort<T> {

    protected T[] swap(T[] arr, int i, int j) {
        T tmp = arr[j];
        arr[j] = arr[i];
        arr[i] = tmp;
        return arr;
    }    
    
}

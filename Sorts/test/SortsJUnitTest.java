/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import sorts.BubbleSort;
import sorts.EvenOddSort;
import sorts.ShakeSort;

/**
 *
 * @author potter
 */
public class SortsJUnitTest {
    Integer[] src2=null;
    Integer[] goal2=null;
    Integer[] src1=null;
    Integer[] goal1=null;
    
    public SortsJUnitTest() {
    }
    
    @Before
    public void setUp() {
        src2 = new Integer[]{2, 1, 4, 5, 10, 0, -5, -7};
        goal2 = new Integer[]{-7, -5, 0, 1, 2, 4, 5, 10};
        src1 = new Integer[]{2, 1, 4, 5, 0, -5, -7};
        goal1 = new Integer[]{-7, -5, 0, 1, 2, 4, 5};        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void bubble() {
        Assert.assertArrayEquals("Bubble sort odd length", goal1, (new BubbleSort<Integer>()).sort(src1));
        Assert.assertArrayEquals("Bubble sort even length", goal2, (new BubbleSort<Integer>()).sort(src2));        
    }
    
    @Test
    public void shake() {
        Assert.assertArrayEquals("Shake sort odd length", goal1, (new ShakeSort<Integer>()).sort(src1));
        Assert.assertArrayEquals("Shake sort even length", goal2, (new ShakeSort<Integer>()).sort(src2));        
    }    
    
    @Test
    public void evenodd() {
        Assert.assertArrayEquals("Evenodd sort odd length", goal1, (new EvenOddSort<Integer>()).sort(src1));
        Assert.assertArrayEquals("Evenodd sort even length", goal2, (new EvenOddSort<Integer>()).sort(src2));        
    }       
}

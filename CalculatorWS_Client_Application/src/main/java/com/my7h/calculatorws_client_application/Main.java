/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.my7h.calculatorws_client_application;

/**
 *
 * @author potter
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Integer add = add(3, 4);
        System.out.println("Result = " + add);
    }

    private static Integer add(int i, int j) {
        Integer result=null;
        try { // Call Web Service Operation
            org.me.calculator.CalculatorWS_Service service = new org.me.calculator.CalculatorWS_Service();
            org.me.calculator.CalculatorWS port = service.getCalculatorWSPort();
            
            result = port.add(i, j);
//            System.out.println("Result = " + result);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            System.err.println("Exception: "+ex);
        }
        return result;
    }
}

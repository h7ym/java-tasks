/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListImpl;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author my7h
 */
public class SingleLinkedList<T> extends AbstractList<T> {
    private class ListItem<T> {

        public ListItem() {
        }
        public ListItem(T item) {
            this.item=item;
        }        
        public ListItem(T item, ListItem<T> next) {
            this.item=item;
            this.next = next;
        }                
        private T item=null;
        private ListItem<T> next=null;
    }
    
    private ListItem<T> head=null;
    private ListItem<T> top=head;
    
    private class SllIterator implements Iterator<ListItem<T>> {

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private ListItem<T> iterPtr = null;

        public SllIterator() {
            iterPtr = SingleLinkedList.this.head;
        }
        
        @Override
        public boolean hasNext() {
            return iterPtr!=null;// && iterPtr.next!=null;
        }

        @Override
        public ListItem<T> next() {
            if(hasNext()) {
                ListItem<T> result = iterPtr;
                iterPtr=iterPtr.next;
                return result;
            }
            else throw new NoSuchElementException();
        }    
    }
    
    private class IteratorImpl implements Iterator<T> {
        private final SllIterator iter = new SllIterator();
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean hasNext() {
            return iter.hasNext();
        }

        @Override
        public T next() {
            if(hasNext()) {
                return iter.next().item;
            }
            else throw new NoSuchElementException();
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }    
    }
    
    @Override
    public T get(int index) {        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int size() {
        if(head==null) return 0;
        else {
            Iterator<T> iterator = this.iterator();
            int result = 0;
            while(iterator.hasNext()) {
                iterator.next();
                result++;
            }
            return result;
        }            
    }

    @Override
    public void add(int index, T element) {
        if(index==0) {
//            ListItem<T> top = head;
            head = new ListItem<>(element, head);
        }
        else {
            Iterator<ListItem<T>> iter = new SllIterator();
            int ctr = index;
            ListItem<T> current = null;
            while(iter.hasNext() && (ctr--)>0) {
                current = iter.next();
            }
            current.next = new ListItem<>(element, current.next);
        }
    }

    @Override
    public boolean add(T e) {
        if(top==null) {
            head = new ListItem<>(e);
            top = head;
        }
        else {
            top.next = new ListItem<>(e);
            top = top.next;
        }
        return true;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorImpl();
    }
    
    
}

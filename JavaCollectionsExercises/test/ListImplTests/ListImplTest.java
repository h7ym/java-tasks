/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListImplTests;

import ListImpl.SingleLinkedList;
import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author my7h
 */
public class ListImplTest {
    
    public ListImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void SingleLinkedListSize() {
        SingleLinkedList sll = new SingleLinkedList();
        assertEquals(0, sll.size());
        sll.add((Integer)1);
        System.out.println(sll.size());
        assertEquals(1, sll.size());
        sll.add((Integer)1);
        System.out.println(sll.size());
        assertEquals(2, sll.size());  
        
        sll = new SingleLinkedList();
        final int limit = 40_000_000;
        for(int i=0; i<limit; i++)
            sll.add((Integer)i);
        assertEquals(limit, sll.size());  
    }
    
    @Test
    public void SingleLinkedListIterate() {
        SingleLinkedList sll = new SingleLinkedList();
        sll.add((Integer)1);
        sll.add((Integer)2);
        sll.add((Integer)3);
        sll.add((Integer)4);
        Iterator iterator = sll.iterator();
        assertNotNull(iterator);
        assertTrue("has first", iterator.hasNext());
        assertEquals(1, iterator.next());
        assertTrue("has second", iterator.hasNext());
        assertEquals(2, iterator.next());
        assertTrue("has third", iterator.hasNext());
        assertEquals(3, iterator.next());        
    }    
}
